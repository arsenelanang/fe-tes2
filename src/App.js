import React from 'react';
import productsJson from './data/products.json';

// model product
class product {
  index;
  isSale;
  isExclusive;
  price;
  productImage;
  productName;
  size;
}

class App extends React.Component {
  
  constructor(props) {
    super(props);

    // initialize state
    this.state = {
      products: [],
      productsJson : [],   
      filterSize: ''
    };
  }

  componentDidMount()
  {
    this.setState({products: productsJson });
    this.setState({productsJson: productsJson });
  }

  // function for filter data products array by size
  filterSize = (e) => {

    //init array data products and new array for data by filter
    let products = this.state.productsJson;
    let arrayProductsFilter = new Array(); 

    // loop with filter by matching size of products
    products.map((row) => {
      var tempProduct = new product();
      tempProduct.index = row.index;
      tempProduct.isExclusive = row.isExclusive;
      tempProduct.isSale = row.isSale;
      tempProduct.price = row.price;
      tempProduct.productImage = row.productImage;
      tempProduct.productName = row.productName;
      tempProduct.size = row.size;

      if(e.target.value !== "")
      {
        // push data to array if matching
        if (tempProduct.size.indexOf(e.target.value) > -1) {
          arrayProductsFilter.push(tempProduct);
        } 
      }
      else
      {
        // push data to array if size value filter empty
        arrayProductsFilter.push(tempProduct);
      }
    
    });
    
    // update state value filter and  set data products to show  by filter
    this.setState({
        filterSize : e.target.value,
        products : arrayProductsFilter
    });
  
  }

  render()
  {
    let produtcs = this.state.products;
  
    return(
      <div>
        <div className ="container tittle px-0 ">
          <nav className="navbar navbar-light" style={{backgroundColor : '#e3f2fd'}}>
            <span className="navbar-brand mb-0 h1">Women's top</span>
            <form className="form-inline">
                <select value={this.state.filterSize} onChange={this.filterSize} className="custom-select mr-sm-2" >
                      <option value="">Filter by size</option>
                      <option value="XS">XS</option>
                      <option value="S">S</option>
                      <option value="L">L</option>
                      <option value="XL">XL</option>
                </select>
            </form>
          </nav>
        </div>
        <div className="container">
          <div className="row mb-5">

            {
              // loop data products
              produtcs.map((row) => {
                return(
                  <div key ={row.index} className="col-md-3 col-sm-6 border-col">
                      <img src={require('./images/'+row.productImage)} className="card-img-top" alt={row.productName}/>
                      <div className="card-body pl-0">
                        {row.isExclusive ?  <button type="button" className="btn btn-success rounded-0 btn-sm">Exclusive</button> : ''}
                        {row.isSale ?  <button type="button" className="btn btn-danger rounded-0 btn-sm">Sale</button> : <button type="button" className="btn btn-success rounded-0 btn-sm invisible">blank</button>}
                      </div>
                      <div className="row mb-3">
                        <div className="col-7 col-md-9">{row.productName}</div>
                        <div className="col-4 col-md-3"><div className="float-right"><h5>{row.price}</h5></div></div>
                      </div>
                  </div>
                )
              })
            }

          </div>          
        </div>
        
      </div>
    )
  }

}

export default App;
